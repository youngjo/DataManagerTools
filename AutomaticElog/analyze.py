#!/usr/bin/env python
import os, sys
import json

if len(sys.argv) < 3:
	print "Usage: %s DATATYPE RUNNUMBER" % sys.argv[0]
	sys.exit()

type = sys.argv[1]
if type == "Cosmics": datasetName = "Cosmics"
elif type == "Collisions": datasetName = "SingleMu"
else:
  print "DATATYPE must be Cosmics or Collisions"
  sys.exit()

runNumber = int(sys.argv[2])
print "@@ Reading DQM root file @@"

## Open DQM root file
sys.argv.append("-b")
from ROOT import *

fName = ""
for ff in os.listdir("cache/%d" % runNumber):
  if not ff.startswith("DQM_") or not ff.endswith(".root"): continue
  ff = "cache/%d/%s" % (runNumber, ff)
  if datasetName in ff: fName = ff
  if "RPC" in ff: fNameOnline = ff

f = TFile(fName)
if f == None or not f.IsOpen():
	print "Cannot open root file ", sys.argv[1]
	sys.exit()
#fOnline = TFile(fNameOnline)

ffName = os.path.basename(fName)
if 'RPC' in ffName: # Online DQM
  pdName, sdName = "Online", 'RPC'
else:
  pdName, sdName = ffName.split('__')[-3:-1]

runSummaryPath = "DQMData/Run %d/RPC/Run summary/" % runNumber
nEvent = -99
## Get information for the reports
for key in f.Get(runSummaryPath+"/EventInfo").GetListOfKeys():
  key = key.GetName()
  if not key.startswith("<iEvent>"): continue
  nEvent = int(key[10:-9])
nRPCEvents = f.Get(runSummaryPath+"AllHits/RPCEvents").GetEntries()
nLS = -99
for key in f.Get(runSummaryPath+"/DCSInfo").GetListOfKeys():
  key = key.GetName()
  if not key.startswith("<rpcHV>"): continue
  nLS = int(key[9:-8])
nFATAL = f.Get(runSummaryPath+"FEDIntegrity/FEDFatal").GetEntries()
clsBarrel = f.Get(runSummaryPath+"Muon/SummaryHistograms/ClusterSize_Barrel").GetMean()
clsEndcapP = f.Get(runSummaryPath+"Muon/SummaryHistograms/ClusterSize_Endcap+").GetMean()
clsEndcapM = f.Get(runSummaryPath+"Muon/SummaryHistograms/ClusterSize_Endcap-").GetMean()
clsEndcap = (clsEndcapP+clsEndcapM)/2;
nClBarrel = f.Get(runSummaryPath+"Muon/SummaryHistograms/NumberOfClusters_Barrel").GetMean()
nClEndcapP = f.Get(runSummaryPath+"Muon/SummaryHistograms/NumberOfClusters_Endcap+").GetMean()
nClEndcapM = f.Get(runSummaryPath+"Muon/SummaryHistograms/NumberOfClusters_Endcap-").GetMean()
multBarrel = f.Get(runSummaryPath+"AllHits/SummaryHistograms/Multiplicity_Barrel").GetMean()
multEndcapP = f.Get(runSummaryPath+"Muon/SummaryHistograms/Multiplicity_Endcap+").GetMean()
multEndcapM = f.Get(runSummaryPath+"Muon/SummaryHistograms/Multiplicity_Endcap-").GetMean()
multEndcap = (multEndcapP+multEndcapM)/2

## Chamber quality
nQB, nBxB = 0, 0
barrelNSt, barrelNCh, barrelPdd, barrelFdd, barrelBsh = [], [], [], [], []
barrelBx = []
for w in range(-2, 3):
  hQ  = f.Get(runSummaryPath+("Muon/SummaryHistograms/RPCChamberQuality_Distribution_Wheel%d" % w))
  #hQ  = fOnline.Get(runSummaryPath+("AllHits/SummaryHistograms/RPCChamberQuality_Distribution_Wheel%d" % w))
  nQB += hQ.GetEntries()
  barrelNSt.append(hQ.GetBinContent(3))
  barrelNCh.append(hQ.GetBinContent(4))
  barrelPdd.append(hQ.GetBinContent(5))
  barrelFdd.append(hQ.GetBinContent(6))
  barrelBsh.append(hQ.GetBinContent(7))

  hBx = f.Get(runSummaryPath+("Muon/SummaryHistograms/BxDistribution_Wheel_%d" % w))
  if hBx:
    barrelBx.append(hBx.GetMean())
    nBxB += hBx.GetEntries()
  else: barrelBx.append(-99)

nQE, nBxE = 0, 0
endcapNSt, endcapNCh, endcapPdd, endcapFdd, endcapBsh = [], [], [], [], []
endcapBx = []
for d in range(-4, 5):
  hQ  = f.Get(runSummaryPath+("Muon/SummaryHistograms/RPCChamberQuality_Distribution_Disk%d" % d))
  if hQ == None: continue
  #hQ  = fOnline.Get(runSummaryPath+("AllHits/SummaryHistograms/RPCChamberQuality_Distribution_Wheel%d" % w))
  nQE += hQ.GetEntries()
  endcapNSt.append(hQ.GetBinContent(3))
  endcapNCh.append(hQ.GetBinContent(4))
  endcapPdd.append(hQ.GetBinContent(5))
  endcapFdd.append(hQ.GetBinContent(6))
  endcapBsh.append(hQ.GetBinContent(7))

  hBx = f.Get(runSummaryPath+("Muon/SummaryHistograms/BxDistribution_Disk_%d" % d))
  if hBx:
    endcapBx.append(hBx.GetMean())
    nBxE += hBx.GetEntries()
  else: endcapBx.append(-99)

## RecHit multiplicity
for w in range(-2, 3):
  hO = f.Get(runSummaryPath+("Muon/SummaryHistograms/Occupancy_Roll_vs_Sector_Wheel_%d" % w))
  c = TCanvas("cOccupancyW%d" % w, "Occupancy W%d" % w, 600, 500)
  hO.Draw("COLZ")
  c.Print("cache/%d/%s.png" % (runNumber, c.GetName()))
hO = f.Get(runSummaryPath+"Muon/SummaryHistograms/Occupancy_for_Barrel")
c = TCanvas("cOccupancyB", "Occupancy for Barrel", 600, 500)
hO.Draw("COLZ")
c.Print("cache/%d/%s.png" % (runNumber, c.GetName()))

for d in range(-4, 5):
  hO = f.Get(runSummaryPath+("Muon/SummaryHistograms/Occupancy_Roll_vs_Sector_Endcap_%d" % d))
  if hO == None: continue
  c = TCanvas("cOccupancyE%d" % d, "Occupancy E%d" % d, 600, 500)
  hO.Draw("COLZ")
  c.Print("cache/%d/%s.png" % (runNumber, c.GetName()))
hO = f.Get(runSummaryPath+"Muon/SummaryHistograms/Occupancy_for_Endcap")
c = TCanvas("cOccupancyE", "Occupancy for Endcap", 600, 500)
hO.Draw("COLZ")
c.Print("cache/%d/%s.png" % (runNumber, c.GetName()))

## Analyse NoiseTool
hasNTWeb, hasWBM = False, False
nDisabled, nDead, nEnable, nToDisable = -1, -1, -1, -1
rateBarrel, rateEndcap = -1, -1
if os.path.exists("cache/%d/index.html" % runNumber):
  hasNTWeb = True
  for l in open("cache/%d/index.html" % runNumber).readlines():
    if   "masked." in l: nDisabled = int(l.split()[-2])
    elif "tomask." in l: nToDisable = int(l.split()[-2])
    elif "tounmask." in l: nEnable = int(l.split()[-2])
    elif "deadstrips." in l: nDead = int(l.split()[-2])
  f = TFile("cache/%d/barrel.root" % runNumber)
  if f.IsOpen() and f.Get(" Rolls - WIN"): rateBarrel = f.Get(" Rolls - WIN").GetMean()
  else: hasNTWeb = False
  f = TFile("cache/%d/endcap.root" % runNumber)
  if f.IsOpen() and f.Get(" Rolls - WIN"): rateEndcap = f.Get(" Rolls - WIN").GetMean()
  else: hasNTWeb = False

if not hasNTWeb and os.path.exists("cache/%d/Brate.json" % runNumber):
  hasWBM = True
  jsBRate = json.loads(open("cache/%s/Brate.json" % runNumber).read())
  jsBDisabled = json.loads(open("cache/%s/Bdisabled.json" % runNumber).read())
  jsBDead = json.loads(open("cache/%s/BdeadStrip.json" % runNumber).read())
  jsBToDisable = json.loads(open("cache/%s/BtoDisable.json" % runNumber).read())
  jsBToEnable = json.loads(open("cache/%s/BtoEnable.json" % runNumber).read())

  jsERate = json.loads(open("cache/%s/Erate.json" % runNumber).read())
  jsEDisabled = json.loads(open("cache/%s/Edisabled.json" % runNumber).read())
  jsEDead = json.loads(open("cache/%s/EdeadStrip.json" % runNumber).read())
  jsEToDisable = json.loads(open("cache/%s/EtoDisable.json" % runNumber).read())
  jsEToEnable = json.loads(open("cache/%s/EtoEnable.json" % runNumber).read())

  rateBarrel, rateEndcap = jsBRate['mean'], jsERate['mean']
  nDisabled = jsBDisabled['sum'] + jsEDisabled['sum']
  nDead = jsBDead['sum'] + jsEDead['sum']
  nEnable = jsBToEnable['sum'] + jsEToEnable['sum']
  nToDisable = jsBToDisable['sum'] + jsEToDisable['sum']

## Write elog
runFlag = ""
badReasons = []
if type == "Cosmics":
    if nFATAL != 0: badReasons.append("(nFATAL=%d) != 0" % nFATAL)

    if nRPCEvents < 10000:
        runFlag = "(short run)"
        if (nEnable+nDisabled) >= 0.10*68280: badReasons.append("(nEnable+nDisabled)/68280 >= 10%%, nEnable = %d, nDisabled = %d" % (nEnable, nDisabled))

    else:
        if clsBarrel >= 3.0: badReasons.append("(clsBarrel=%f) >= 3.0" % clsBarrel)
        if nClBarrel >= 5.0: badReasons.append("(nClBarrel=%f) >= 5.0" % nClBarrel)
        if multBarrel >= 6.0: badReasons.append("(multBarrel=%f) >= 6.0" % multBarrel)
        if (nEnable+nDisabled) >= 0.05*68280:
            badReasons.append("(nEnable+nDisabled)/68280 >= 5%%, nEnable = %d, nDisabled = %d" % (nEnable, nDisabled))

elif type == "Collisions":
    if nFATAL != 0: badReasons.append("(nFATAL=%d) != 0" % nFATAL)

    if nLS <= 15:
        runFlag = "(very short run)"
        #if len(badReasons) == 0: runFlag = "NOTBAD,TO BE DOUBLE CHECKED " + runFlag
        #else: runFlag = "BAD" + runFlag

    elif nLS < 200:
        if (nEnable+nDisabled) >= 0.10*(68280+41472):
            badReasons.append("(nEnable+nDisabled)/(68280+41472) >= 10%%, nEnable = %d, nDisabled = %d" % (nEnable, nDisabled))

        runFlag = "(short run)"

    else:
        if (nEnable+nDisabled) >= 0.05*(68280+41472):
            badReasons.append("(nEnable+nDisabled)/(68280+41472) >= 5%%, nEnable = %d, nDiabled = %d" % (nEnable, nDisabled))
        nBadState = sum(barrelNSt)+sum(barrelNCh)+sum(barrelPdd)+sum(barrelFdd)+sum(barrelBsh) \
                  + sum(endcapNSt)+sum(endcapNCh)+sum(endcapPdd)+sum(endcapFdd)+sum(endcapBsh)
        if nBadState >= 0.1*(nQB+nQE): badReasons.append("nBadState = %d >= 10%%" % nBadState)
        if clsBarrel >= 3.0: badReasons.append("clsBarrel = %f >= 3.0" % clsBarrel)
        if clsEndcap >= 3.0: badReasons.append("clsEndcap = %f >= 3.0" % clsEndcap)
        if nClBarrel > 5.0: badReasons.append("nClBarrel = %f > 5.0" % nClBarrel)
        if nClEndcapP > 5.0: badReasons.append("nClEndcap+ = %f > 5.0" % nClEndcapP)
        if nClEndcapM > 5.0: badReasons.append("nClEndcap- = %f > 5.0" % nClEndcapM)
        if multBarrel > 5.0: badReasons.append("multBarrel = %f > 5.0" % multBarrel)
        if multEndcap > 5.0: badReasons.append("multBarrel = %f > 5.0" % multEndcap)

        runFlag = "(long run)"

fLog = open("cache/%d/elog.txt" % runNumber, "w")
print>>fLog, "Run%d %s15 is GOOD/BAD %s" % (runNumber, type, runFlag)
print>>fLog, ("="*100)
print>>fLog, "     Run        Type               RPC Flag             Events"
print>>fLog, "  %d        %s        GOOD/BAD %s                 %d" % (runNumber, type, runFlag, nEvent)
print>>fLog, ("="*100), "\n\n"
print>>fLog, "***** Using Offline DQM /%s/%s/DQM" % (pdName, sdName)
print>>fLog, "      Offline DQM : https://cmsweb.cern.ch/dqm/offline/start?runnr=%d;dataset=/%s/%s/DQMIO;sampletype=offline_data;workspace=RPC;root=RPC/Layouts" % (runNumber, pdName, sdName)
#print>>fLog, "      Online DQM : https://cmsweb.cern.ch/dqm/online/start?runnr=%d;dataset=/Global/Online/ALL;sampletype=online_data;workspace=RPC;root=RPC/Layouts" % (runNumber)
print>>fLog, "*"*40
print>>fLog
print>>fLog, "*"*40
print>>fLog, "Reason for BAD flag (REMOVE THIS SECTION IF THE RUN IS GOOD):"
if len(badReasons) != 0: print>>fLog, ("\n".join(badReasons))
else:
    print>>fLog, "--- Could not find reason to set BAD with the shifter checklist."
    print>>fLog, "--- Before setting the flag, please double check..."
print>>fLog, "Contact DOC for noise, current or temperature problems."
print>>fLog, "LIST ALL PROBLEMS IN THE SUMMARY ELOG, EVEN MINOR ONES"
print>>fLog, "*"*40
print>>fLog
print>>fLog, "1. # of RPC events = %d\n" % nRPCEvents
print>>fLog, "2. # of FED errors = %d\n" % nFATAL
print>>fLog, "3. Noise tool output"
if hasNTWeb or hasWBM:
  if hasNTWeb: print>>fLog, "   https://rpcbackground.web.cern.ch/rpcbackground/Plots/GR2014/run%d/index.html" % runNumber
  if hasWBM: print>>fLog, "   https://cmswbm.web.cern.ch/cmswbm/cmsdb/servlet/RPCSummary?Run=%d&TopMenu=RPCRunSummary2&TopMenu2=ZeroPage&SubMenu=1" % runNumber
  print>>fLog, "   Disabled: %d    Dead:   %d" % (nDisabled, nDead)
  print>>fLog, "   Enable:   %d  To Disable: %d" % (nEnable, nToDisable)
  print>>fLog, "   Average Barrel Noise Rate: %.2f" % (rateBarrel)
  print>>fLog, "   Average Endcap Noise Rate: %.2f" % (rateEndcap)
  print>>fLog, ""
else:
  print>>fLog, "NO NOISE TOOL INFORMATION"
print>>fLog, ""
print>>fLog, "4-1. Barrel_RPCChamberQuality_Distribution"
if nQB == 0:
  print>>fLog, "  N/A"
else:
  print>>fLog, "                               W-2  W-1   W0  W+1  W+2"
  print>>fLog, "   Noisy strip                  %d   %d   %d   %d   %d" % tuple(barrelNSt)
  print>>fLog, "   Noisy Chamber                %d   %d   %d   %d   %d" % tuple(barrelNCh)
  print>>fLog, "   Partially Dead               %d   %d   %d   %d   %d" % tuple(barrelPdd)
  print>>fLog, "   Dead                         %d   %d   %d   %d   %d" % tuple(barrelFdd)
  print>>fLog, "   Bad shape                    %d   %d   %d   %d   %d" % tuple(barrelBsh)
print>>fLog, ""
if type != "Cosmics":
    print>>fLog, "4-2. Endcap_RPCChamberQuality_Distribution"
    if nQE == 0:
      print>>fLog, "  N/A"
    else:
      print>>fLog, "                               D-4  D-3  D-2  D-1  D+1  D+2  D+3  D+4"
      print>>fLog, "   Noisy strip                  %d   %d   %d   %d   %d   %d   %d   %d" % tuple(endcapNSt)
      print>>fLog, "   Noisy Chamber                %d   %d   %d   %d   %d   %d   %d   %d" % tuple(endcapNCh)
      print>>fLog, "   Partially Dead               %d   %d   %d   %d   %d   %d   %d   %d" % tuple(endcapPdd)
      print>>fLog, "   Dead                         %d   %d   %d   %d   %d   %d   %d   %d" % tuple(endcapFdd)
      print>>fLog, "   Bad shape                    %d   %d   %d   %d   %d   %d   %d   %d" % tuple(endcapBsh)
    print>>fLog, ""

print>>fLog, "5-1. BunchCrossing Distribution/Wheel (mean value)"
if nBxB == 0: print>>fLog, " N/A"
else:
  print>>fLog, "                               W-2  W-1   W0  W+1  W+2"
  print>>fLog, "                              %.1f %.1f %.1f %.1f %.1f" % tuple(barrelBx)
print>>fLog, ""
if type != "Cosmics":
    print>>fLog, "5-2. BunchCrossing Distribution/Disk (mean value)"
    if nBxE == 0: print>>fLog, " N/A"
    else:
      print>>fLog, "                               D-4  D-3  D-2  D-1  D+1  D+2  D+3  D+4"
      print>>fLog, "                              %.1f %.1f %.1f %.1f %.1f %.1f %.1f %.1f" % tuple(endcapBx)
    print>>fLog, ""

print>>fLog, "6-1. Average number of clusters per event Barrel = %.1f" % nClBarrel
print>>fLog, "     Average cluster size Barrel                 = %.1f" % clsBarrel
if type != "Cosmics":
    print>>fLog, "6-2. Average number of clusters per event Endcap = %.1f" % ((nClEndcapP+nClEndcapM)/2)
    print>>fLog, "     Average cluster size Endcap                 = %.1f" % clsEndcap
    print>>fLog, ""
print>>fLog, "7-1. Average number of single hits Barrel        = %.1f" % multBarrel
if type != "Cosmics": print>>fLog, "7-2. Average number of single hits Endcap        = %.1f" % multEndcap
print>>fLog, ""
print>>fLog, "8-1. Barrel Occupancy  (TO BE CHECKED)"
if type != "Cosmics": print>>fLog, "8-2. Endcap Occupancy  (TO BE CHECKED)"
fLog.close()

print "\n"
print open("cache/%d/elog.txt" % runNumber).read()
