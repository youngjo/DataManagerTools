# Automatic E-Log tool for RPC Data Managers

Up-to-date shift instructions can be found in the following links:
  * https://twiki.cern.ch/twiki/bin/viewauth/CMS/RPCOfflineShifts\_Run2
  * https://twiki.cern.ch/twiki/bin/view/CMS/RPCAutomaticElog

## Installation
  1. Log on a Console 3 or 4 in the CMS Centre.
  2. Open a terminal and run the following commands

```bash
ssh -XY <YOUR_AFS_LOGIN>@lxplus.cern.ch
cd /tmp/$USER # or any other directory with enough space. The output file can be large
mkdir AutomaticElog
cd AutomaticElog
wget https://gitlab.cern.ch/CMSRPCDPG/DataManagerTools/raw/master/AutomaticElog/run.sh
wget https://gitlab.cern.ch/CMSRPCDPG/DataManagerTools/raw/master/AutomaticElog/analyze.py
chmod +x run.sh
chmod +x runMakeTable.sh
```
